#include<iostream>
#include<cstdlib>
#include<ctime>
#include<stack>
#include<fstream>
#define length 100
using namespace std;
typedef long long ll;
ofstream fout("equation.txt");
char Op[] = {'+', '-', '*', '/'};
int rights;
int wrong;

struct num{
    ll numerator, denominator;
    num(){numerator = 0; denominator = 1;}
    num(int n) {numerator = n; denominator = 1;}
    num(int n,int d) {numerator = n; denominator = d;}
    
void operator = (num x)
    {
        numerator = x.numerator;
        denominator = x.denominator;
    }
};
#define maxl 1005
char nifix[maxl], post[maxl];
char ans[maxl];
int cnt_right, cnt_wrong;
bool error;
num res, rst;


    class fraction
    {
     private:
        int above;        
        int below;        
        void reduction();            
        fraction makeCommond(fraction); 
 
    public:
        fraction()
        {             
        }
       fraction add(fraction);     
       fraction sub(fraction);     
       fraction mul(fraction);      
       fraction div(fraction);      
       int display(int,int);              
       void setvalue(int ,int);               
    };

//约分//

      void fraction::reduction()
      {
        int i,comdiv,small,max;
        if(above<below)
        {
          small=above;
          max=below;
         }
        else
        {
          small=below;
          max=above;
        }
       for(i=small;i>1;i--)
       {
          if((small%i==0 )&(max%i==0) )
          break;
       }
         comdiv=i;           

          if(i!=0)
          {
              above/=i;
              below/=i;
           }
    }

 //分数通分//

    fraction fraction::makeCommond(fraction frac)
    {
       int b1=below,b2=frac.below, m,s;
       if(b1>b2)
       {
        m=b1%b2;
        s=b2;
       }
        else
        {
          m=b2%b1;
          s=b1;
        }
       while(m>0)
       {
         int res=s%m;
          s=m,m=res;
       }
       int small=(b1*b2)/s;
       above=above*(small/below);
       frac.above=frac.above*(small/frac.below);
       below=small;
       frac.below=small;
       return frac;
    }
   //分数相加//

    fraction fraction::add(fraction fr)
    {
          fraction myFraction;
          myFraction.above=above*fr.below+fr.above*below;
          myFraction.below=below*fr.below;
          myFraction.reduction();
          return myFraction;
    }
//分数相减//

     fraction fraction::sub(fraction fr)
     {
           fraction myFraction;
           myFraction.above=above*fr.below-fr.above*below;
           myFraction.below=below*fr.below;
           myFraction.reduction();
           return myFraction;
    }

  //分数相乘//

    fraction fraction::mul(fraction fr)
    { 
          fraction myFraction;
          myFraction.above=above*fr.above;
          myFraction.below=below*fr.below;
          myFraction.reduction();
          return myFraction;
    } 
//分数相除//

    fraction fraction::div(fraction fr)
    {
        fraction myFraction; 
        myFraction.above=above*fr.below;
        myFraction.below=below*fr.above;
        myFraction.reduction();
        return myFraction;
    }

 //分数答案的输入判断//

    int fraction::display(int a,int b)
    {
    
        if((a==above)&&(b==below))
        {
            cout<<"正确"<<endl;
            rights=rights+1;
        }
        else
        {
            cout<<"错误"<<endl;
            wrong=wrong+1;
        }
        return rights,wrong;
    } 

  //分数的赋值//

     void fraction::setvalue(int sj1,int sj3)           
     { 
        above=sj1;
        below=sj3;
    }
//无分数，无余数答案判断//
    
     int answer(int a[],int i)
    {
        int ans;
        
        cout<<"请输入答案:"<<endl;
        cin>>ans;
          if(ans==a[i]) 
          {
              cout<<"正确"<<endl;
              rights=rights+1;
            }
          else
          {
              cout<<"错误"<<endl;
              wrong=wrong+1;
          }
          return rights,wrong;
     
    }
 //无分数，有余数答案判断//
    int answer_1(int a[],int i,int b[])
    {
        int ans,yushu;
        
        cout<<"请输入商:"<<endl;
        cin>>ans;
        cout<<"输入余数"<<endl;
        cin>>yushu;
          if((ans==a[i])&&(yushu=b[i])) 
          {
              cout<<"正确"<<endl;
               rights=rights+1;
            }
          else
          {
              cout<<"错误"<<endl;
              wrong=wrong+1;
          }
          return rights,wrong;
     
    }
 //产生带括号式子//
    void create(int maxn)
    {
        
        if(!fout)                        
        {
          cerr << "open error!" << endl;
         exit(1);
        }
    int lengt;
    do{
        lengt = rand()%8;
    }while(lengt < 2);
    bool div = false;                       
    int brack_cnt = 0;                       
    ll num, op;
    for (int i = 1; i < lengt; i++)        
    {
        if (div)                            
        {
            div = false;
            do{
                num = rand()%maxn;
            }while(num == 0);
            cout<< num;
            fout<< num;
            
        }
        else
        {
            num= rand()%maxn;
            fout <<num;
            cout<<num;
        }
        int tmpcnt = brack_cnt;
        for (int j = 0; j < tmpcnt; j++)      
        {
            if ((rand()%5) > 2)             
            {
                brack_cnt--;
                
                fout << ")";
                cout<<")";
            }
        }
        
        op = rand()%4;                     
        fout << Op[op];
        cout << Op[op];
        if (op == 3)                      
            div = true;
        
        if (!(rand()%3))                 
        {
            fout << "(";
            cout<<"(";
            brack_cnt++;
            num= rand()%maxn;
            fout <<num;
            cout<<num;
            op = rand()%4;
            fout << Op[op];
            cout<<Op[op];
            if (op == 3)
                div = true;
        }
    }
    if (div)                            
    {
        div = false;
        do{
            num = rand()%maxn;
        }while(num == 0);
        fout << num;
        cout<< num;    
    }
    else
    {
        num=rand()%maxn;
        fout << num;
        cout<<num;
        
    }
    while(brack_cnt--)           
    {
        fout << ")";
        cout << ")";
    }
        cout<<"=";
        fout<< endl;
        cout<<endl;
        
}
    bool isNum(char x)       
{
    return (x >= '0' && x <= '9');
}

bool isOp(char x)         
{
    return (x == '+' || x == '-' || x == '*' || x == '/' || x == '(' || x == ')');
}

int priority(char x)       
{
    if (x == '-' || x == '+')
        return 1;
    else if (x == '*' || x == '/')
        return 2;
    else if (x == '(')
        return 0;
    else
        return -1;
}
bool nifix_to_post()
{
    memset(post, 0, sizeof(post));
    stack<char> s;                             
    bool havenum = false;                        
    int tmp = 0, pos = 0;                       
    for (int i = 0; nifix[i] != '\0'; i++)     
    {
        if (isNum(nifix[i]))                    
        {
            havenum = true;
            tmp = tmp*10 + (nifix[i]-'0');
        }
        else if (isOp(nifix[i]))              
        {
          
            if (isOp(nifix[i-1]) && nifix[i-1] != ')' && nifix[i] != '(') 
                return true; 
           
            if (havenum)
            {
                havenum = false;
                post[pos++] = tmp + '0';
                tmp = 0;
            }
           
            if (nifix[i] == ')')
            {
                if (s.empty())                 
                    return true;
                while(s.top() != '(')
                {
                    post[pos++] = s.top();
                    s.pop();
                    if (s.empty())              
                        return true;
                }
                s.pop();
            }
            else if (nifix[i] == '(')         
                s.push(nifix[i]);
            else                               
            {
                while (!s.empty() && priority(nifix[i]) <= priority(s.top()))
                {
                    post[pos++] = s.top();
                    s.pop();
                }
                s.push(nifix[i]);
            }
        }
        else                                   
            return true;
    }
  
    if (havenum)
    {
        havenum = false;
        post[pos++] = tmp + '0';
        tmp = 0;
    }
  
    while(!s.empty())
    {
        if (s.top() == '(')                     
            return true;
        post[pos++] = s.top();
        s.pop(); 
    }
    return false;
}
ll gcd(ll m, ll n)
{
    ll    tmp;
    tmp = m % n;
    while(tmp)
    {
        m = n;
        n = tmp;
        tmp = m % n;
    }
    return n;
}
bool cal_result()
{
    stack<num> s;                               
    for (int i = 0; i < (strlen(post)); i++)      
    {
        if (!isOp(post[i]))                     
        {
            num tmp(post[i]-'0', 1);
            s.push(tmp);
        }
        else
        {
          
            if (s.empty())
                return true;
            num b = s.top(); s.pop();
            if (s.empty())
                return true;
            num a = s.top(); s.pop();
            num c;

            if (post[i] == '+')                 
            {
                c.numerator = a.numerator * b.denominator + b.numerator * a.denominator;
                c.denominator = a.denominator * b.denominator;
            }
            else if (post[i] == '-')          
            {
                c.numerator = a.numerator * b.denominator - b.numerator * a.denominator;
                c.denominator = a.denominator * b.denominator;
            }
            else if (post[i] == '*')         
            {
                c.numerator = a.numerator * b.numerator;
                c.denominator = a.denominator * b.denominator;
            }
            else if (post[i] == '/')          
            {
                if (b.numerator == 0)         
                    return true;
                c.numerator = a.numerator * b.denominator;
                c.denominator = a.denominator * b.numerator;
            }
            else                           
                return true;
            if (c.numerator != 0)              
            {
                ll div = gcd(c.denominator, c.numerator);
                c.denominator /= div;
                c.numerator /= div;
            }
            s.push(c);                          
        }
    }
    if (s.size() > 1)                           
        return true;
    res = s.top();                              
    s.pop();
    if (res.denominator < 0)                  
    {
        res.numerator = -res.numerator;
        res.denominator = -res.denominator;
    }
    return false;    
}

bool trans_ans()
{
    int i = 0;
    ll tmp = 0; 
    bool num_flag = false, deno_flag = false;
     
    if (ans[i] == '-')
    {
        num_flag = true;
        i++;
    }
  
    while(isNum(ans[i]))
    {
        tmp = tmp * 10 + (ans[i] - '0');
        i++;
    }
   
    if (num_flag)
        tmp = -tmp;
   
    rst.numerator = tmp;
   
    rst.denominator = 1;
    tmp = 0;  
    if (ans[i] == '/')
    { 
        if (ans[++i] == '-')
        {
            deno_flag = true;
            i++;
        }
        while(isNum(ans[i]))
        {
            tmp = tmp * 10 + (ans[i] - '0');
            i++;
        }
        if (deno_flag)
            tmp = -tmp;

        rst.denominator = tmp;
    }
    if (rst.denominator == 0)
        return true;
    if (i != strlen(ans))
        return true;   
    if (rst.denominator < 0)
    {
        rst.numerator = -rst.numerator;
        rst.denominator = -rst.denominator;
    }
    if (num_flag && deno_flag)
        rst.denominator = 0;
    return false;
}
int main()
{
    fraction frac,frac2;
    int sj1,sj2,sf=1,sj3,sj4,r,j=1;//定义随机数及算符
    int above,below;
    int Num;//题数
    char nc;//运算数选择
    char sfchose;//算符选择
    char yschose;//余数选择
    char jf;//减法结果选择
    int qznum;//取值范围
    int ans_2[length];//存放正确的答案
    int ans_4[length]={0};//存放正确的余数
    srand((unsigned)time(NULL)); //srand函数是以现在系统时间作为时间种子产生随机数
    cout<<"欢迎使用四则运算自动出题系统"<<endl;
    cout<<"请对以下内容进行初始化设置："<<endl;
    cout<<"选择年级一年级（输入'1'）或二年级（输入'2')或三年级（输入'3')或四年级（输入'4')或五年级（输入'5')"<<endl;
    cin>>nc;
    if(nc=='1'){{
        cout<<"产生一年级四则运算，选择难度低输入6，中输入7,高输入8"<<endl;
    cin>>nc;
    if(nc=='6'){
        cout<<"你选择了简单模式，请输入a，建议不要设置乘除法"<<endl;}
    if(nc=='7'){
        cout<<"你选择了中等模式，请输入a，建议设置乘除法"<<endl;}
    if(nc=='8'){
        cout<<"你选择了难模式，请输入a，建议题目量大于50，要有乘除"<<endl;}
    }}
    if(nc=='2'){{
    cout<<"进入二年级四则运算，选择难度低输入6，中输入7,高输入8"<<endl;}
    cin>>nc;
    if(nc=='6'){
        cout<<"你选择了简单模式，请输入a，建议不要设置乘除法"<<endl;}
    if(nc=='7'){
        cout<<"你选择了中等模式，请输入a，建议设置乘除法"<<endl;}
    if(nc=='8'){
        cout<<"你选择了难模式，请输入a，题目量大于50，要有乘除"<<endl;}
    }
    if(nc=='3'){{
    cout<<"进入三年级四则运算，请选择难度：低选择'6';中选择'7';高的模式选择'8'"<<endl;}
    cin>>nc;
    if(nc=='6'){
        cout<<"你选择了简单模式，请输入b，进入后，建议题目小于30"<<endl;}
    if(nc=='7'){
        cout<<"你选择了中等模式，请输入b，进入后，建议题目大于30小于50"<<endl;}
    if(nc=='8'){
        cout<<"你选择了难模式，请输入b，进入后，建议题目量大于50"<<endl;}
    }
    if(nc=='4'){{
    cout<<"进入四年级四则运算，请选择难度：低选择'6';中选择'7';高的模式选择'8'"<<endl;}
    cin>>nc;
    if(nc=='6'){
        cout<<"你选择了简单模式，请输入b，进入后建议题目小于30"<<endl;}
    if(nc=='7'){
        cout<<"你选择了中等模式，请输入b，进入后建议题目大于30小于50"<<endl;}
    if(nc=='8'){
        cout<<"你选择了难模式，请输入b，进入后题目量大于50"<<endl;}
    }
    if(nc=='5'){{
    cout<<"进入五年级四则运算，请选择难度：(1)难度低的模式选择'6';(2)中等选择'7'(3)难度高的模式选择'8'"<<endl;}
    cin>>nc;
    if(nc=='6'){
        cout<<"你选择了简单模式，建议输入b，进入后，建议题目小于30"<<endl;}
    if(nc=='7'){
        cout<<"你选择了中等模式，建议输入b，进入后，建议题目大于30小于50"<<endl;}
    if(nc=='8'){
        cout<<"你选择了难模式，建议输入b，进入后，建议题目量大于50"<<endl;}
    }
    cout<<"请输入难度模式（a.二个运算数 b.多个运算数）"<<endl;
    cin>>nc;
    if(nc=='a')
    {
        cout<<"请输入打印题数："<<endl;
    cin>>Num;
    cout<<"请选择是否有乘除法?（y/n）"<<endl;
    cin>>sfchose;
    cout<<"请输入一个值确定算式中数值取值范围："<<endl;
    cin>>qznum;
    cout<<"减法结果中出现负数吗？（y/n）";
    cin>>jf;

    if(sfchose=='y')
    {
        cout<<"请选择除法有无余数?（y/n）"<<endl;
        cin>>yschose;
    }
        cout<<"********************************"<<endl;

    for(int i=0;i<Num;i++)
    {
            sj1=rand()%qznum;
            sj2=rand()%qznum;
            sj3=rand()%qznum;
            sj4=rand()%qznum;
            if(sfchose=='n')//无乘除法
            {
                 sf=rand()%4;         
            }
            if(sfchose=='y')//有乘除法
            {
                  sf=rand()%8; 
            }
            switch(sf)
                {        
                     case 0:
                       cout<<sj1<<"+"<<sj2<<"=     "<<endl;
                       ans_2[i]=sj1+sj2;
                       answer(ans_2,i);

                     break;
                    case 1:
                       if(jf=='n')
                       {
                           if(sj1<sj2)
                           {
                              cout<<sj2<<"-"<<sj1<<"=    "<<endl;
                              ans_2[i]=sj2-sj1;
                              answer(ans_2,i);
                        
                           }
                           else
                           {
                               cout<<sj1<<"-"<<sj2<<"=    "<<endl;
                               ans_2[i]=sj1-sj2;
                               answer(ans_2,i);
                           }
                        }
                       else
                       {
                            cout<<sj1<<"-"<<sj2<<"=    "<<endl;
                            ans_2[i]=sj1-sj2;
                            answer(ans_2,i);
                        
                       } 
                       break;
                  case 2:
                       if(sj1>sj3)
                       {
                           r=sj1;
                           sj1=sj3;
                           sj3=r;
                       }
                       if(sj2>sj4)
                       {
                           r=sj2;
                           sj2=sj4;
                           sj4=r;
                       }
                     cout<<"("<<sj1<<"/"<<sj3<<")"<<"+"<<"("<<sj2<<"/"<<sj4<<")=     "<<endl;
                     frac.setvalue(sj1,sj3);
                     frac2.setvalue(sj2,sj4);
                     cout<<"输入分子和分母（用空格隔开，结果化到最简）";
                     cin>>above>>below;
                     frac.add(frac2).display(above,below); 

                    break;
                  case 3:
                        if(sj1>sj3)
                        {
                            r=sj1;
                            sj1=sj3;
                            sj3=r;
                        }
                        if(sj2>sj4)
                        {
                            r=sj2;
                            sj2=sj4;
                            sj4=r;
                        }
                        if(jf=='n')
                        {
                            if((sj1/sj3)<(sj2/sj4))
                            { 
                                cout<<"("<<sj2<<"/"<<sj4<<")"<<"-"<<"("<<sj1<<"/"<<sj3<<")=     "<<endl;
                                frac.setvalue(sj2,sj4);
                                frac2.setvalue(sj1,sj3);
                                cout<<"输入分子和分母（用空格隔开，结果化到最简）";
                                cin>>above>>below;
                                frac.sub(frac2).display(above,below); 

                            }
                            else
                            {
                                cout<<"("<<sj1<<"/"<<sj3<<")"<<"-"<<"("<<sj2<<"/"<<sj4<<")=     "<<endl;
                                frac.setvalue(sj1,sj3);
                                frac2.setvalue(sj2,sj4);
                                cout<<"输入分子和分母（用空格隔开，结果化到最简）";
                                cin>>above>>below;
                                frac.sub(frac2).display(above,below); 
                            }
                        }
                         else
                         {
                                cout<<"("<<sj1<<"/"<<sj3<<")"<<"-"<<"("<<sj2<<"/"<<sj4<<")=     "<<endl;
                                frac.setvalue(sj1,sj3);
                                frac2.setvalue(sj2,sj4);
                                cout<<"输入分子和分母（用空格隔开，结果化到最简）";
                                cin>>above>>below;
                                frac.sub(frac2).display(above,below); 
                         }
                       break;
                   case 4:
                      cout<<sj1<<"*"<<sj2<<"="<<endl;
                      ans_2[i]=sj1*sj2;
                      answer(ans_2,i);
                    break;
                   case 5:
                       if(sj2==0)
                       {
                           i=i-1;
                        
                       }
                       else if(yschose=='n')
                       {
                           if(sj1%sj2==0)
                           {
                               cout<<sj1<<"/"<<sj2<<"="<<endl;
                               ans_2[i]=sj1/sj2;
                               answer(ans_2,i);

                        
                           }
                           else 
                           {
                               i=i-1;
                             
                           }
                       }
                       else if(yschose=='y')
                       {
                             if(sj1%sj2!=0)
                           {
                               cout<<sj1<<"/"<<sj2<<"="<<endl;
                               ans_2[i]=sj1/sj2;
                               ans_4[i]=sj1-sj2*ans_2[i];
                               answer_1(ans_2,i,ans_4);
                    
                           }
                           else 
                           {
                               i=i-1;
                        
                           }

                       }
                       break;
                   case 6:
                    if(sj1>sj3)
                    {
                        r=sj1;
                        sj1=sj3;
                        sj3=r;
                    }
                    if(sj2>sj4)
                    {
                        r=sj2;
                        sj2=sj4;
                        sj4=r;
                    }
                     cout<<"("<<sj1<<"/"<<sj3<<")"<<"*"<<"("<<sj2<<"/"<<sj4<<")=     "<<endl;
                     frac.setvalue(sj1,sj3);
                     frac2.setvalue(sj2,sj4);
                     cout<<"输入分子和分母（用空格隔开，结果化到最简）";
                     cin>>above>>below;
                     frac.mul(frac2).display(above,below); 
                       break;
                   case 7:
                       if(sj1>sj3)
                       {
                           r=sj1;
                           sj1=sj3;
                           sj3=r;
                       }
                     if(sj2>sj4)
                     {
                        r=sj2;
                        sj2=sj4;
                        sj4=r;
                     }
                       cout<<"("<<sj1<<"/"<<sj3<<")"<<"/"<<"("<<sj2<<"/"<<sj4<<")=     "<<endl;
                       frac.setvalue(sj1,sj3);
                       frac2.setvalue(sj2,sj4);
                       cout<<"输入分子和分母（用空格隔开，结果化到最简）";
                       cin>>above>>below;
                       frac.div(frac2).display(above,below); 
                       break;
                   default:
                   break;
                }
    }
     cout<<"共做对  "<<rights<<"  道题"<<endl;
     cout<<"共做错  "<<wrong<<"  道题"<<endl;
    }
    else
    {
        
        cout<<"请输入出题数目：";
        cin >>Num ;
        cout<<"请输入一个值确定算式中数值取值范围："<<endl;
        cin>>qznum;
        cout<<"******************************************"<<endl;
        while(Num--)
        {
            create(qznum);
        }
        //计数器请0 
    cnt_right = cnt_wrong = 0;
    ifstream infile;
    infile.open("equation.txt", ios::in);
      if(infile.is_open() == false)
      {
         cerr << "open error!" << endl;
         exit(1);
      }
        
    while( infile >> nifix)
    {
        error = nifix_to_post();           
        if (error)                           
        { 
            cout << "读取式子出错!" << endl;
            return 0; 
        }                    
        error = cal_result();              
        if (error)                          
        { 
            cout << "计算中出错..." << endl;
            return 0; 
        }
        
        cout << "请输入答案: ";
        cin >> ans;                           
        error = trans_ans();              
        if (error)                            
        { 
            cout << "输入不合法答案!" << endl;
            return 0; 
        }
       
        if ((rst.denominator == res.denominator && rst.numerator == res.numerator) || (rst.numerator == res.numerator && rst.numerator == 0))
        {
            cnt_right++;
            cout << "正确!" << endl;
        }
         
        else
        {
            cnt_wrong++;
            cout << "错误. 答案是 ";
            if (res.denominator == 1)
                cout << res.numerator << "." << endl;
            else
                cout << res.numerator << "/" << res.denominator << "." << endl;
        }
    }
    cout << "统计结果..." << endl;
     
    cout << "你答了" << cnt_right+cnt_wrong << " 道题 ";
    cout << "正确数：" << cnt_right << "   错误数： " << cnt_wrong << " 。" << endl;
    infile.close();
 
    }
    system("pause");
     return 0;
}